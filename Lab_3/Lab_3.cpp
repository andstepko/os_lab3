// Lab_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <String.h>
#include "iostream"
#include "ConIO.h"
#include <locale.h>

typedef enum {GP_SUCCESS, GP_10, GP_REPEAT, GP_BAD, GP_STRONG} ErrorCode;
typedef enum {DIGIT, SL, CL, SIGN, BAD} Types;
//const TCHAR endl[] = _T("\r\n");
TCHAR endl[] = _T("\r\n");


Types GetType(TCHAR c)
{
	TCHAR signs[] = _T("_-=+*/\|.,!@#$%^&*()");
	if(c >= _T('0') && c <= _T('9'))
		return DIGIT;
	else if(c >= _T('a') && c <= _T('z'))
		return SL;
	else if(c >= _T('A') && c <= _T('Z'))
		return CL;
	else if(_tcschr(signs, c) != 0)
		return SIGN;
	return BAD;
}

ErrorCode CheckPass(TCHAR password[])
{
	//ErrorCode code;
	//int count = 0;
	int len = _tcslen(password);
	int mask = 0;
	if(len < 8)
		return GP_STRONG;
	for(int i = 0; i < len; i++)
	{
		Types type = GetType(password[i]);
		if(type == BAD)
			return GP_BAD;
		mask |= 1 << type;
	}
	if(mask == 15)
		return GP_SUCCESS;
	return GP_STRONG;
}

bool WritePrompt(HANDLE h, TCHAR invitation[])
{
	//TCHAR endl[] = _T("\r\n");
	DWORD writtenBytes;
	bool b = WriteConsole(h, invitation, _tcslen(invitation), &writtenBytes, 0);
	if(b)
		//b = WriteConsole(h, endl, 2, &writtenBytes, 0);
		b = WriteConsole(h, _T("\r\n"), 2, &writtenBytes, 0);
	return b;
}

bool ReadPass(HANDLE h1, bool seen, HANDLE h2, TCHAR pass[], int length)
{
	TCHAR star = _T('*');
	TCHAR empty = _T('');
	DWORD d;
	TCHAR buffer;
	int i;
	bool b;

	for(i = 0; i < length-1; i++)
	{
		b = ReadConsole(h1, &buffer, 1, &d, 0);
		if(buffer == _T('\r'))
			break;
		else if(buffer == _T('\b')) // Backspace
		{
			pass[i-1] = empty;
			i -= 2;
			CONSOLE_SCREEN_BUFFER_INFO cursor;
			GetConsoleScreenBufferInfo(h2, &cursor);
			COORD location = cursor.dwCursorPosition;
			location.X--;
			SetConsoleCursorPosition(h2, location);
			b = WriteConsole(h2, &empty, 1, &d, 0);
			SetConsoleCursorPosition(h2, location);
		}
		else // adequate char
		{
			if(seen)
				b = WriteConsole(h2, &buffer, 1, &d, 0);
			else // invisible mode
				b= WriteConsole(h2, &star, 1, &d, 0);
			pass[i] = buffer;
		}
	}
	pass[i] = 0;
	b = WriteConsole(h2, endl, 2, &d, 0);
	return b;
}

void Password(bool seen, TCHAR pass[], int length)
{
	DWORD mode;
	bool b;
	TCHAR invitation[] = _T("Input your password(no non-Latin letters): ");
	TCHAR errorBad[] = _T("Sorry, your password uses forbidden symbols!");
	TCHAR errorStrong[] = _T("Sorry, your password is not strong enough! Be sure that your password is not shorter thatn 8 symbols and you use small and capital letters, digits and symbols.");
	TCHAR successMessage[] = _T("Password is OK.");
	HANDLE handleIn, handleOut;
	ErrorCode errorCode;

	bool bCreate = AllocConsole();
	handleIn = GetStdHandle(STD_INPUT_HANDLE);
	handleOut = GetStdHandle(STD_OUTPUT_HANDLE);

	b = GetConsoleMode(handleIn, &mode);

	if(b)
	{
		if(seen)
		{
			SetConsoleMode(handleIn, (mode | ENABLE_ECHO_INPUT) & ~ENABLE_INSERT_MODE & ~ENABLE_LINE_INPUT & ~ENABLE_QUICK_EDIT_MODE);
			//SetConsoleMode(handleOut, (ENABLE_ECHO_INPUT & ~ENABLE_INSERT_MODE | ENABLE_LINE_INPUT) & ~ENABLE_QUICK_EDIT_MODE);
		}
		else // invisible mode
		{
			SetConsoleMode(handleIn, ~ENABLE_ECHO_INPUT & ~ENABLE_INSERT_MODE & ~ENABLE_LINE_INPUT & ~ENABLE_QUICK_EDIT_MODE);
		}
	}

	if(b)
	{
		while(true)
		{
			b = WritePrompt(handleOut, invitation);
			if(b)
			{
				ReadPass(handleIn, seen, handleOut, pass, length);
			}
			if(b)
			{
				errorCode = CheckPass(pass);
			}
			if(b)
			{
				if(errorCode == GP_BAD)
				{					 
					_tprintf(_T("%s\n"), errorBad);
				}
				else if (errorCode == GP_STRONG)
				{
					 _tprintf(_T("%s\n"), errorStrong);
				}
				else // success
				{
					 _tprintf(_T("%s\n"), successMessage);
					 break;
				}
				_tprintf(_T("%s\n"));
			}
		}// end of while(true)
	}
	getch();
	if(bCreate) // I had to create a new Console
		FreeConsole();
	else
		SetConsoleMode(handleOut, mode); // return old mode of console
}


int getFields(unsigned int extCode, int &code, int &subSystem, int &owner, int &error)
{
	code = extCode & 0xFFFF; // 0..16
	subSystem = (extCode >> 16) & 0xFFFF;
	owner = (extCode >> 29) & 1;
	error = (extCode >> 30) & 3;
	return code;
}
unsigned int getExtCode(int code, int subSystem, int owner, int error)
{
	unsigned int result  = error << 30;
	result |= owner << 29;
	result |= subSystem << 16;
	result |= code;
	return result;
}

void differentInfoOutput()
{
	printf("Number infromation: ");
	printf("%s\n", "123");
	printf("Char infromation: ");
	printf("abc");
}

void mesBoxCheck()
{
	TCHAR textEng[] = _T("And here is the text!");
	TCHAR captionEng[] = _T("This is the caption");

	TCHAR textRus[] = _T("� ��� ��� �����!");
	TCHAR captionRus[] = _T("��� ���������");

	TCHAR textUkr[] = _T("� �� �����!");
	TCHAR captionUkr[] = _T("�� ��������� �");

	/*_tsetlocale(LC_ALL, _T("English"));
	MessageBox(0, textEng, captionEng, MB_ICONINFORMATION);
	MessageBox(0, textEng, captionEng, MB_ICONWARNING);
	MessageBox(0, textEng, captionEng, MB_ICONWARNING);
	MessageBox(0, textEng, captionEng, MB_ICONQUESTION);

	_tsetlocale(LC_ALL, _T("�������"));
	MessageBox(0, textRus, captionRus, MB_ICONINFORMATION);
	MessageBox(0, textRus, captionRus, MB_ICONWARNING);
	MessageBox(0, textRus, captionRus, MB_ICONWARNING);
	MessageBox(0, textRus, captionRus, MB_ICONQUESTION);*/

	_tsetlocale(LC_ALL, _T("���������"));
	MessageBox(0, textUkr, captionUkr, MB_ICONINFORMATION);
	//MessageBox(0, textUkr, captionUkr, MB_ICONWARNING);
	//MessageBox(0, textUkr, captionUkr, MB_ICONWARNING);
	//MessageBox(0, textUkr, captionUkr, MB_ICONQUESTION);
}


void NumbersIO()
{
	DWORD mode;
	bool b = true;
	HANDLE handleIn, handleOut;

	TCHAR invitation[] = _T("Insert an integer: \n");
	TCHAR resultMessage[] = _T("Your integer: ");
	TCHAR errorMessage[] = _T("Error.\n");

	bool bCreate = AllocConsole();
	handleIn = GetStdHandle(STD_INPUT_HANDLE);
	handleOut = GetStdHandle(STD_OUTPUT_HANDLE);

	while(b)
	{
		DWORD charsWritten;
		DWORD charsRead;
		TCHAR *input = new TCHAR[20];		

		DWORD CharsWritten;
		DWORD CharsRead;
		b = WriteConsole(handleOut, invitation, _tcslen(invitation), &charsWritten, 0); // invite
		b = ReadConsole(handleIn, input, 20, &charsRead, 0); // read
		int result = _ttoi(input); // Convert

		WriteConsole(handleOut, resultMessage, _tcslen(resultMessage), &charsWritten, 0);
		//WriteConsole(handleOut, input, charsRead, &charsWritten, 0);
		printf("%d", result);

		WriteConsole(handleOut, endl, 2, &charsWritten, 0);
		//getch();
	}// end of while(true)
	if(!b)
		_tprintf(errorMessage);
}


int _tmain(int argc, _TCHAR* argv[])
{
	TCHAR password[42];

	//differentInfoOutput();
	//mesBoxCheck();

	//Password(true, password, 21);
	Password(false, password, 21);

	//NumbersIO();

	for(int i = 0; i < 20; i++)
	{
		int r = (rand() << 17) | (rand() << 2) | (rand() >> 13);
		int code;
		int subSystem;
		int owner;
		int error;
		getFields(r, code, subSystem, owner, error);
		int r1 = getExtCode(code, subSystem, owner, error);

		if(r == r1)
		{
			_tprintf(_T("OK"));
		}
		else
		{
			_tprintf(_T("Error!"));
		}
	}
	getch();

	return 0;
}

